The programs are automated markers for instructors of Computer Applications courses (and others) that use Microsoft Office. They include:
Word Grader
PowerPoint Grader
Excel Grader
Access Grader

Click on the installer link and then "View raw" to download installer.

See papers for program details.

These programs have reached end-of-life.
They are provided freely AS IS and there are no plans for updates.